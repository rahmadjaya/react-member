import React from 'react';
// import { Link, NavLink } from 'react-router-dom';


class InsertMember extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            first_name: '',
            last_name: '',
            jabatan: '',
            email: '',
            gender: '',
            notif: ''
        }
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	componentDidMount(){
		
		
    }
    
    Username(e){
        this.setState({username: e.target.value.trim()})
    }
    FirstName(e){
        this.setState({first_name: e.target.value})
    }
    LastName(e){
        this.setState({last_name: e.target.value})
    }
    Jabatan(e){
        this.setState({jabatan: e.target.value})
    }
    Email(e){
        this.setState({email: e.target.value})
    }
    Gender(e){
        this.setState({gender: e.target.value})
    }
    handleSubmit(e){
        e.preventDefault();
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
                username: this.state.username, 
                first_name: this.state.first_name, 
                last_name: this.state.last_name, 
                jabatan: this.state.jabatan, 
                email: this.state.email, 
                gender: this.state.gender
            })
        };
        fetch('http://localhost:3000/bo/v1/insertmember', requestOptions)
            .then(response => response.json())
            .then(data =>{
                this.setState({notif: data.message})
            });
    
    }
    Notif(){
        if(this.state.notif !== '' ){
            setTimeout(
                function() {
                    this.setState({notif: ''});
                    this.setState({username: ''})
                    this.setState({first_name: ''})
                    this.setState({last_name: ''})
                    this.setState({jabatan: ''})
                    this.setState({email: ''})
                    this.setState({gender: ''})
                }
                .bind(this),
                3000
            );
            return (
                <div className="notif">
                    {this.state.notif}
                </div>
            )
        }
        
    }
    render() {
        return (
            <div className="container">
                {this.Notif()}                
                <div className="caption">
                    | <i>RAHMAD WIJAYA</i>
                    <h1>WELCOME WORLD</h1>
                </div>
                <div className="insert-member">
                
                    <form onSubmit={this.handleSubmit} autoComplete="off" id="formMember">
                        <div className="group">
                            <label htmlFor="username">Username</label>
                            <input type="text" id="username" placeholder="*Username"  name="username" value={this.state.username} onChange={this.Username.bind(this)} required/>
                        </div>
                        <div className="group">
                            <label htmlFor="first_name">Nama Depan</label>                        
                            <input type="text" id="first_name" placeholder="*Nama Depan" name="first_name" value={this.state.first_name} onChange={this.FirstName.bind(this)} required/>
                        </div>
                        <div className="group">
                            <label htmlFor="last_name">Nama Belakang</label>                        
                            <input type="text" id="last_name" placeholder="*Nama Belakang" name="last_name" value={this.state.last_name} onChange={this.LastName.bind(this)} required/>
                        </div>
                        <div className="group">
                            <label htmlFor="jabatan">Jabatan</label>                        
                            <input type="text" id="jabatan" placeholder="*Jabatan" name="jabatan" value={this.state.jabatan} onChange={this.Jabatan.bind(this)} required/>
                        </div>
                        <div className="group">
                            <label htmlFor="email">Email</label>                        
                            <input type="email" id="email" placeholder="*Email" name="email" value={this.state.email} onChange={this.Email.bind(this)} required/>
                        </div>
                        <div className="group">
                            <label htmlFor="gender">Jenis Kelamin</label>                        
                            <input type="text" id="gender" placeholder="*Gender" name="gender" value={this.state.gender} onChange={this.Gender.bind(this)} required/>
                        </div>
                        <div className="group text-right">
                            <button type="submit" className="btn">&#43; Tambah</button>
                        </div>
                    </form>
                </div>
            
            </div>
            
        )
    }
}


export default InsertMember;