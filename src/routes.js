import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import Header from './header';
import InsertMember from './insertmember';
import Members from './members';
const history = createBrowserHistory();

export const routes = (
    <Router history={ history }>
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={InsertMember}/>
                <Route path="/insertmember" component={InsertMember}/>     
                <Route path="/members" component={Members}/>     
                {/* <Route path="*" component={NotFound}/> */}
            </Switch>
        </div>
    </Router>
);