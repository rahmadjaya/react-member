import React from 'react';
// import { Link, NavLink } from 'react-router-dom';


class Members extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            members: [],
            modaledit: false,
            useredit : {
                first_name: '',
                last_name: '',
                jabatan: '',
                email: '',
                gender: ''
            },
			modaldel: false,
            userdelete : '',
            first_name: '',
            last_name: '',
            jabatan: '',
            email: '',
            gender: '',
            notif: ''
        }
		this.handleSubmit = this.handleSubmit.bind(this);

	}
	componentDidMount(){
        fetch('http://localhost:3000/bo/v1/members')
            .then((response) => response.json())
            .then((res)=>{
                this.setState({members: res})
            })
    }
    clickModalDelete(data){
        this.setState({userdelete: data})
        this.setState({modaldel: true})
	}
    ModalDel(){
		if(this.state.modaldel === true){
			return(
				<div className="modal modal-form">
                    <div className="form-delete">
                        <div className="label">Apakah anda yakin ?</div>
                        <button onClick={() => this.deleteMember()}>HAPUS</button>
                        <button onClick={() => this.BatalDelete()}>TIDAK</button>
                    </div>
				</div>
			)
		}
	}
    deleteMember(){
        this.setState({modaldel: false})
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        };
        fetch('http://localhost:3000/bo/v1/delmember/'+this.state.userdelete, requestOptions)
            .then(response => response.json())
            .then(data =>  {
                    this.setState({notif: data.message})
                }
            );
    }
    BatalDelete(){
        this.setState({modaldel: false})
	}
    clickModalEdit(data){
        this.setState({useredit: data})
        this.setState({modaledit: true})
	}
    BatalEdit(){
        this.setState({modaledit: false})
	}
    ModalEdit(){
		if(this.state.modaledit === true){
			return(
				<div className="modal modal-form">
                    <form onSubmit={this.handleSubmit}>
                        <div className="label">Edit Member</div>
                        <div className="group">
                            <label>Username</label>
                            <input type="text"  name="username" value={this.state.useredit.username} disabled/>
                        </div>
                        <div className="group">
                            <label>Nama Depan</label>                        
                            <input type="text" name="first_name" value={this.state.useredit.first_name} onChange={this.FirstName.bind(this)}   required/>
                        </div>
                        <div className="group">
                            <label>Nama Belakang</label>                        
                            <input type="text" name="last_name" value={this.state.useredit.last_name} onChange={this.LastName.bind(this)}  required/>
                        </div>
                        <div className="group">
                            <label>Jabatan</label>                        
                            <input type="text" name="jabatan" value={this.state.useredit.jabatan} onChange={this.Jabatan.bind(this)} required/>
                        </div>
                        <div className="group">
                            <label>Email</label>                        
                            <input type="email" name="email"  value={this.state.useredit.email} onChange={this.Email.bind(this)} required/>
                        </div>
                        <div className="group">
                            <label>Jenis Kelamin</label>                        
                            <input type="text" name="gender"  value={this.state.useredit.gender} onChange={this.Gender.bind(this)} required/>
                        </div>
                        <button type="submit" className="btn">EDIT</button>
                        <button className="btn" onClick={() => this.BatalEdit()}>TIDAK</button>
                    </form>
				</div>
			)
		}
	}
    handleSubmit(e){
        this.setState({modaledit: false})
        e.preventDefault();
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
                first_name: this.state.useredit.first_name, 
                last_name: this.state.useredit.last_name, 
                jabatan: this.state.useredit.jabatan, 
                email: this.state.useredit.email, 
                gender: this.state.useredit.gender
            })
        };
        fetch('http://localhost:3000/bo/v1/editmember/'+this.state.useredit.username, requestOptions)
            .then(response => response.json())
            .then(data =>{
                this.setState({notif: data.message})
            } );
    
    }
    FirstName(e){
        this.setState(prevState => ({
            useredit: {                   
                ...prevState.useredit,    
                first_name: e.target.value     
            }
        }))
    }
    LastName(e){
        this.setState(prevState => ({
            useredit: {                   
                ...prevState.useredit,    
                last_name: e.target.value     
            }
        }))
    }
    Jabatan(e){
        this.setState(prevState => ({
            useredit: {                   
                ...prevState.useredit,    
                jabatan: e.target.value     
            }
        }))
    }
    Email(e){
        this.setState(prevState => ({
            useredit: {                   
                ...prevState.useredit,    
                email: e.target.value     
            }
        }))
    }
    Gender(e){
        this.setState(prevState => ({
            useredit: {                   
                ...prevState.useredit,    
                gender: e.target.value     
            }
        }))
    }
    Notif(){
        if(this.state.notif !== '' ){
            fetch('http://localhost:3000/bo/v1/members')
                .then((response) => response.json())
                .then((res)=>{
                    this.setState({members: res})
                })
            setTimeout(
                function() {
                    this.setState({notif: ''});
                }
                .bind(this),
                3000
            );
            return (
                <div className="notif">
                    {this.state.notif}
                </div>
            )
        }
        
    }
    render() {
        return (
            <div className="members">
                {this.Notif()}                
                <div className="container">
                    <div className="list-member">
                    {
                        this.state.members.map((data, i)=>{
                            return (
                                <div className="sub-member" key={data._id}>
                                    <div className="username">{data.username}</div>
                                    <div className="first_name">{data.first_name} {data.last_name}</div>
                                    <div className="jabatan">{data.jabatan}</div>
                                    <div className="email">{data.email}</div>
                                    <div className="gender">{data.gender}</div>
                                    <button className="btn" onClick={() => this.clickModalEdit(data)}>Edit</button>
                                    <button className="btn" onClick={() => this.clickModalDelete(data.username)}>Delete</button>

                                </div>
                            )
                        })
                    }
                    </div>
                </div>
                {this.ModalDel()}
                {this.ModalEdit()}
            </div>
            
        )
    }
}


export default Members;